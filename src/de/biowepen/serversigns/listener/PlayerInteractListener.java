package de.biowepen.serversigns.listener;

import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import de.biowepen.api.multilingual.Multilingual;
import de.biowepen.api.player.PlayerHandler;
import de.biowepen.api.server.ServerHandler;
import de.biowepen.api.server.ServerInfo;
import de.biowepen.serversigns.SignInfo;
import de.biowepen.serversigns.Utils;

public class PlayerInteractListener implements Listener {
	
	@EventHandler
	public void teleportToServer(PlayerInteractEvent e)
	{
		if(e.getAction() == Action.RIGHT_CLICK_BLOCK)
		{
			if(e.getClickedBlock().getState() instanceof Sign)
			{
				if(Utils.isSign(e.getClickedBlock().getLocation()))
				{
					SignInfo info = Utils.getSignInfo(e.getClickedBlock().getLocation());
					ServerInfo si = ServerHandler.getServerInfo(info.getId());
					if(!si.isOnline())
					{
						e.getPlayer().sendMessage(Multilingual.getMessage("serversigns_not_joinable", PlayerHandler.getPlayerLanguage(e.getPlayer()), null));
						return;
					}
					PlayerHandler.teleportPlayerToServer(e.getPlayer(), si.getBungeename());
				}
			}
		}
	}

}
