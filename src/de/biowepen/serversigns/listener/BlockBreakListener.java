package de.biowepen.serversigns.listener;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import de.biowepen.api.multilingual.Multilingual;
import de.biowepen.api.player.PlayerHandler;
import de.biowepen.serversigns.Utils;

public class BlockBreakListener implements Listener {
	
	@EventHandler
	public void destroySign(BlockBreakEvent e)
	{
		Location loc = e.getBlock().getLocation();
		if(Utils.isSign(loc))
		{
			if(!e.isCancelled())
			{
				Utils.removeSign(loc);
				e.getPlayer().sendMessage(Multilingual.getMessage("serversigns_remove", PlayerHandler.getPlayerLanguage(e.getPlayer()), null));	
			}
		}
	}

}
