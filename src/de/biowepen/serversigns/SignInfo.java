package de.biowepen.serversigns;

import org.bukkit.Location;

public class SignInfo {
	
	private int id;
	private Location loc;
	
	public SignInfo(int id, Location loc)
	{
		this.id = id;
	    this.loc = loc;
	}

	public int getId() 
	{
		return this.id;
	}

	public void setId(int id) 
	{
		this.id = id;
	}

	public Location getLoc()
	{
		return this.loc;
	}

	public void setLoc(Location loc) 
	{
		this.loc = loc;
	}
}
