package de.biowepen.serversigns;

import org.bukkit.block.Sign;

public class SignLayout {
	
	public String line1,line2,line3,line4;
	
	public SignLayout(String line1, String line2, String line3, String line4)
	{
		this.line1 = line1;
		this.line2 = line2;
		this.line3 = line3;
		this.line4 = line4;
	}
	
	public void apply(Sign s)
	{
		s.setLine(0, line1);
		s.setLine(1, line2);
		s.setLine(2, line3);
		s.setLine(3, line4);
		s.update();
	}

}
