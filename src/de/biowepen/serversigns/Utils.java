package de.biowepen.serversigns;

import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.InvalidConfigurationException;

public class Utils {
	
	public static void addSign(int id, Location loc)
	{
		ServerSigns.cfg.set("Servers." + convertLocationToString(loc), id);
		try {
			ServerSigns.cfg.save(ServerSigns.file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		updateList();
	}
	
	public static void removeSign(Location loc)
	{
		ServerSigns.cfg.set("Servers." + convertLocationToString(loc), null);
		try {
			ServerSigns.cfg.save(ServerSigns.file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		updateList();
	}
	
	public static void updateList()
	{
		try {
			ServerSigns.cfg.load(ServerSigns.file);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
		ServerSigns.signs.clear();
		try {
			for(String locString : ServerSigns.cfg.getConfigurationSection("Servers").getKeys(false))
			{
				Location loc = convertStringToLocation(locString);
				int id = ServerSigns.cfg.getInt("Servers." + locString);
				SignInfo info = new SignInfo(id, loc);
				
				ServerSigns.signs.add(info);
			}
		} catch(NullPointerException e) {}
	}
	
	public static boolean isSign(Location loc)
	{
		for(SignInfo info : ServerSigns.signs)
		{
			if(locationEqualsTo(info.getLoc(), loc))
			{
				return true;
			}
		}
		return false;
	}
	
	public static boolean locationEqualsTo(Location loc, Location equals)
	{
		if(loc.getWorld().getName().equals(equals.getWorld().getName()))
		{
			if(loc.getBlockX() == equals.getBlockX())
			{
				if(loc.getBlockY() == equals.getBlockY())
				{
					if(loc.getBlockZ() == equals.getBlockZ())
					{
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public static SignInfo getSignInfo(Location loc)
	{
		for(SignInfo info : ServerSigns.signs)
		{
			if(locationEqualsTo(info.getLoc(), loc))
			{
				return info;
			}
		}
		return null;
	}
	
	public static String convertLocationToString(Location loc)
	{
		return loc.getWorld().getName() + "_" + loc.getBlockX() + "_" + loc.getBlockY() + "_" + loc.getBlockZ();
	}
	
	public static Location convertStringToLocation(String locString)
	{
		String[] split = locString.split("_");
		return new Location(Bukkit.getWorld(split[0]), Double.valueOf(split[1]), Double.valueOf(split[2]), Double.valueOf(split[3]));
	}
	
	
}
