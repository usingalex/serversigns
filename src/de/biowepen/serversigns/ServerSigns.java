package de.biowepen.serversigns;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.block.Sign;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import de.biowepen.api.server.ServerHandler;
import de.biowepen.api.server.ServerInfo;
import de.biowepen.serversigns.listener.BlockBreakListener;
import de.biowepen.serversigns.listener.PlayerInteractListener;
import de.biowepen.serversigns.listener.SignChangeListener;

public class ServerSigns extends JavaPlugin {
	
	  public static File file = new File("plugins/ServerSigns/signs.yml");
	  public static FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
	  
	  public static File configFile = new File("plugins/ServerSigns/config.yml");
	  public static FileConfiguration configCFG = YamlConfiguration.loadConfiguration(configFile);
	  
	  public static ArrayList<SignInfo> signs = new ArrayList<SignInfo>();
	  public static int thread;
	  
	  public static SignLayout layout_Raw = null;
	  public static SignLayout layout_PluginOffline = new SignLayout("", "§cPLUGIN", "§cOFFLINE", "");
	  public static SignLayout layout_ServerOFFLINE = new SignLayout("", "§cSERVER", "§cOFFLINE", "");

	  public void onEnable()
	  {
		  setupConfigs();
		  Utils.updateList();
		  
		  Bukkit.getPluginManager().registerEvents(new BlockBreakListener(), this);
		  Bukkit.getPluginManager().registerEvents(new PlayerInteractListener(), this);
		  Bukkit.getPluginManager().registerEvents(new SignChangeListener(), this);
		  
		  updateSigns();
	  }
	  
	  public void onDisable()
	  {
		  for(SignInfo info : signs)
		  {
			  Sign s = (Sign)info.getLoc().getBlock().getState();
			  layout_PluginOffline.apply(s);
		  }
	  }
	  
	  private void setupConfigs()
	  {
		  try {
			  cfg.save(file);
		  } catch(IOException e) {
			  e.printStackTrace();
		  }
		  
		  configCFG.options().header("Use {0} for the Displayname, {1} for the first motd \n {2} for the Second motd, {3} for the Playercount, and {4} for the MaxPlayercount");
		  configCFG.addDefault("layout.line1", "║ {0} ║");
		  configCFG.addDefault("layout.line2", "{1}");
		  configCFG.addDefault("layout.line3", "{2}");
		  configCFG.addDefault("layout.line4", "{3}/{4}");
		  configCFG.options().copyDefaults(true);
		  
		  try {
			configCFG.save(configFile);
		  } catch (IOException e) {
		 	e.printStackTrace();
		  }
		  
		  String line1 = configCFG.getString("layout.line1");
		  String line2 = configCFG.getString("layout.line2");
		  String line3 = configCFG.getString("layout.line3");
		  String line4 = configCFG.getString("layout.line4");
		  
		  layout_Raw = new SignLayout(line1, line2, line3, line4);
		  
	  }
	  
	  public void updateSigns()
	  {
		  thread = Bukkit.getScheduler().scheduleSyncRepeatingTask(getInstance(), new Runnable() {
			
			@Override
			public void run() {
				for(SignInfo info : signs)
				{
					Sign s = (Sign)info.getLoc().getBlock().getState();
					ServerInfo si = ServerHandler.getServerInfo(info.getId());
					
					if(!si.isOnline())
					{
						layout_ServerOFFLINE.apply(s);
						return;
					}
					
					String motd1 = "";
					String motd2 = "";
					
					String[] motdSplit = si.getMotd().split("~");
					motd1 = motdSplit[0];
					if(motdSplit.length > 1)
					{
						motd2 = motdSplit[1];
					}
					
					
					Object[] args = new Object[]{si.getDisplayname(), motd1, motd2, si.getPlayercount(), si.getMaxplayers()};
					
				    MessageFormat line1Pattern = new MessageFormat(ChatColor.translateAlternateColorCodes('&', layout_Raw.line1));
					MessageFormat line2Pattern = new MessageFormat(ChatColor.translateAlternateColorCodes('&', layout_Raw.line2));
					MessageFormat line3Pattern = new MessageFormat(ChatColor.translateAlternateColorCodes('&', layout_Raw.line3));
					MessageFormat line4Pattern = new MessageFormat(ChatColor.translateAlternateColorCodes('&', layout_Raw.line4));
					
					SignLayout layout = new SignLayout(line1Pattern.format(args), line2Pattern.format(args), line3Pattern.format(args), line4Pattern.format(args));
					
					layout.apply(s);
				}
			}
			
		}, 40L, 25L);
	  }
	  
	  public static Plugin getInstance()
	  {
	    return Bukkit.getPluginManager().getPlugin("ServerSigns");
	  } 
	  
}
