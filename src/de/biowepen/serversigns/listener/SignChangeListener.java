package de.biowepen.serversigns.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

import de.biowepen.api.multilingual.Multilingual;
import de.biowepen.api.player.PlayerHandler;
import de.biowepen.api.server.ServerHandler;
import de.biowepen.serversigns.Utils;

public class SignChangeListener implements Listener {
	
	@EventHandler
	public void createSign(SignChangeEvent e)
	{
		if(e.getLine(0).equalsIgnoreCase("ssign"))
		{
			if(e.getPlayer().hasPermission("ServerSigns.Create"))
			{
				try {
					int id = Integer.valueOf(e.getLine(1));
					if(ServerHandler.dbContainsServer(id))
					{
						Utils.addSign(id, e.getBlock().getLocation());
						e.getPlayer().sendMessage(Multilingual.getMessage("serversigns_create", PlayerHandler.getPlayerLanguage(e.getPlayer()), null));
					} else {
						e.getBlock().breakNaturally();
						e.getPlayer().sendMessage(Multilingual.getMessage("serversigns_noserver", PlayerHandler.getPlayerLanguage(e.getPlayer()), null));
					}
				} catch(NumberFormatException ex) {
					e.getBlock().breakNaturally();
					e.getPlayer().sendMessage(Multilingual.getMessage("please_type_a_number", PlayerHandler.getPlayerLanguage(e.getPlayer()), null));
				}	
			} else {
				e.getBlock().breakNaturally();
				e.getPlayer().sendMessage(Multilingual.getMessage("no_permission", PlayerHandler.getPlayerLanguage(e.getPlayer()), null));
			}
		}
	}

}
